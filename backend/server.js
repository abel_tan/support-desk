const path = require("path")
const express = require("express")
const dotenv = require("dotenv").config()
const colors = require("colors")
const { errorHandler } = require("./middleware/errorMiddleware")
const PORT = process.env.PORT || 5000
const connectDB = require("./config/db")

// Connect to database
connectDB()

const app = express()
//middleware to allow express to work with additional datatypes
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

// Routes
app.use("/api/users", require("./routes/userRoutes"))
app.use("/api/tickets", require("./routes/ticketRoutes"))

// Serve Frontend
if (process.env.NODE_ENV === "production") {
  // Set build folder as static
  app.use(express.static(path.join(__dirname, "../frontend/build")))

  app.get("*", (req, res) =>
    res.sendFile(__dirname, "../", "frontend", "build", "index.html")
  )
} else {
  app.get("/", (req, res) => {
    res.status(200).json({ message: "Welcome!" })
  })
}

// Express error handlers for formatting errors for response
app.use(errorHandler)

app.listen(PORT, () => console.log(`Server started on port ${PORT}`))
