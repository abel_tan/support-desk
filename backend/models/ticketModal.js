const mongoose = require("mongoose")

const ticketSchema = mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "User"
    },
    product: {
      type: String,
      required: [true, "Please select a product"],
      enum: ["iPhone", "Samsung", "Xiaomi", "Nokia"]
    },
    description: {
      type: String,
      required: [true, "Please describe of the issue"]
    },
    status: {
      type: String,
      required: true,
      enum: ["new", "open", "closed"]
    }
  },
  {
    timestamps: true
  }
)

module.exports = mongoose.model("Ticket", ticketSchema)
