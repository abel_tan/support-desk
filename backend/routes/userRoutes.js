const express = require("express")
const router = express.Router()

const {registerUser, loginUser, getMe} = require("../controllers/userController")

const {protect} = require("../middleware/authMiddleware")

router.post("/", registerUser)
router.post("/login", loginUser)
// By adding a middleware function it does the first function followed by second essentially performing authentication in such a case for a route
router.post("/me", protect, getMe) 

module.exports = router