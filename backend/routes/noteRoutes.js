const express = require("express")
const router = express.Router({ mergeParams: true })
const { getNotes, addNote } = require("../controllers/noteController")

const { protect } = require("../middleware/authMiddleware")

// By adding a middleware function it does the first function followed by second essentially performing authentication in such a case for a route
router.route("/").get(protect, getNotes).post(protect, addNote)

module.exports = router
